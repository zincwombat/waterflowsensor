FROM zincwombat/arm32v6-python:1.1

WORKDIR /usr/src/app/

COPY requirements.txt ./
#RUN pip install --upgrade pip
RUN pip install --no-cache-dir -i http://kaon.coomoora:3141/packages/stage --trusted-host kaon.coomoora -r requirements.txt

COPY . .

CMD [ "python", "./FlowSensor/main.py" ]
