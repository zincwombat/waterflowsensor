import logging
import os

import toml
from dotenv import load_dotenv
from zincwombat.mqtt import Mqtt


load_dotenv("config/.env")
config = toml.load("FlowSensor/config/config.toml")


def init_logger(the_config: dict):
    if 'log_level' not in the_config:
        the_config['log_level'] = "INFO"
    if 'log_name' not in the_config:
        the_config['log_name'] = "zw-flow"

    the_logger = logging.getLogger(the_config['log_name'])
    the_logger.setLevel(the_config['log_level'])
    ch = logging.StreamHandler()
    ch.setLevel(the_config['log_level'])
    formatter = logging.Formatter(
        '%(asctime)-15s [%(name)s] - %(levelname)s: %(module)s:%(funcName)s:%(lineno)d - %(message)s')
    ch.setFormatter(formatter)
    the_logger.addHandler(ch)
    the_logger.propagate = False
    return the_logger


logger = init_logger(config['logger'])
mqtt = Mqtt(config['mqtt'])
os.environ['GPIOZERO_PIN_FACTORY'] = 'pigpio'
uname = os.uname()
flow_host = uname.nodename
logger.info('WaterFlowSensor starting on host: {} ({}) {} {}'
            .format(uname.nodename, uname.machine, uname.sysname, uname.release))


