import json
import os
import signal
import sys
from datetime import datetime
from time import sleep, time_ns

from dotenv import load_dotenv
from zincwombat.mqtt import Mqtt

from FlowSensor import logger, mqtt, config, flow_host
from FlowSensor.sensor import PulseHandler

load_dotenv("./config/.env")

DEBUG_ENABLED = os.getenv('DEBUG', None)
MQTT_TOPIC = os.getenv('MQTT_TOPIC', None)


def pulse_handler(pin):
    logger.debug("pin [%s] when_released callback", pin)
    payload = mk_json()
    mqtt.publish(payload, MQTT_TOPIC)


def on_log(client, obj, level, string):
    if DEBUG_ENABLED:
        logger.debug(string)


def mk_json():
    dict_msg = {
        "pulse": 1,
        "local_time": datetime.now(tz=None).isoformat(),
        "time": time_ns(),
        "host": flow_host
    }
    payload = dict_msg
    logger.debug("Payload is: {%s}", payload)
    return json.dumps(dict_msg)


class ExitHandler:
    def __init__(self, the_pulse_handler: PulseHandler, the_broker: Mqtt, the_logger):
        self._mqtt = the_broker
        self._sensor = the_pulse_handler
        self._logger = the_logger
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self._mqtt.disconnect()
        self._logger.warning('exiting on signum {}'.format(signum))
        if isinstance(self._sensor, PulseHandler):
            self._sensor.exit(signum)
        sys.exit()


def main():
    try:
        flow_sensor = PulseHandler(config, mqtt, logger)
        ExitHandler(flow_sensor, mqtt, logger)
        logger.info("Water Flow Sensor started")
        # mqtt.connect()
        while True:
            sleep(1)
    except KeyboardInterrupt:
        logger.error("shutdown requested: exiting")

    except Exception as e:
        logger.error("Exception: {%s}", e)

    finally:
        logger.warning('executing finally clause')

    sys.exit(0)


if __name__ == '__main__':
    main()
