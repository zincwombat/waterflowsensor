**WaterFlow Sensor**

Python script designed to run on Raspberry Pi to send a pulse to an
MQTT broker each time a Hall Effect water flow sensor triggers a rising/falling edge
on a specified GPIO pin.

This script relies on the GPIOZero library https://gpiozero.readthedocs.io/en/stable/ and the pigpio Library http://abyz.me.uk/rpi/pigpio/pigpiod.html

###### Configuration:

Environment variables are set in `config/.env` file as per the following:

**HOST** the hostname of the pi

**MQTT_HOST** the hostname/IP address of the MQTT broker

**MQTT_TOPIC** the MQTT topic to be used, e.g. `myhome/waterflow`

**MQTT_PORT** the port that the MQTT broker is listening on, defaults to 1883

**DEBUG** if set, will provide more verbose logging (e.g. `DEBUG=1`)

**GPIO_LED_ALIVE** GPIO pin connected to a LED which toggles every 1 second when system alive (default None)

**GPIO_LED_FLOW** GPIO pin connected to a LED which toggles whenever a flow sensor rising edge is detected (default None)

**GPIO_LED_MQTT** GPIO pin connected to a LED which is ON when connected to the MQTT broker (default None)

**GPIO_FLOW_SENSOR** GPIO pin connected to the hall effect flow sensor (MANDATORY)
