from dataclasses import dataclass
from datetime import datetime
from time import time_ns
from typing import Optional


@dataclass
class SensorReading:
    pulse: int = 1
    localtime: Optional[datetime] = None
    time: Optional[int] = None

    def __post_init__(self):
        self.localtime = datetime.now(tz=None).isoformat()
        self.time = time_ns()
