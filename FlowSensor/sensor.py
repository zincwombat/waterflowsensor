import json
import os
from datetime import datetime
from logging import Logger
from time import time_ns
from typing import Dict, Optional

from zincwombat.mqtt import Mqtt
from gpiozero import Button as Sensor
from FlowSensor.leds import Led

# TODO record start time, total pulses etc
from FlowSensor.models import SensorReading


class PulseHandler:
    def __init__(self, config: Dict, mqtt: Mqtt, logger: Logger):

        _sensor_config = config['sensor']

        self._logger = logger
        self._mqtt = mqtt
        self.sensor: Sensor = None
        self.led_flow: Optional[Led] = None
        self.led_mqtt: Optional[Led] = None
        self.led_alive: Optional[Led] = None

        if 'pulse_input' not in _sensor_config.keys():
            raise EnvironmentError("no pulse input pin defined")

        if 'mqtt_topic' not in _sensor_config.keys():
            raise EnvironmentError("no mqtt publish topic provided")

        if 'led_alive' in _sensor_config.keys():
            self.led_alive = Led(_sensor_config['led_alive'])
            self._logger.info('using GPIO pin {} as led_alive output'.format(_sensor_config['led_alive']))
        else:
            self._logger.warning('led_alive output is NOT enabled')

        if 'led_mqtt' in _sensor_config.keys():
            self.led_mqtt = Led(_sensor_config['led_mqtt'])
            self._logger.info('using GPIO pin {} as led_mqtt output'.format(_sensor_config['led_mqtt']))
        else:
            self._logger.warning('led_mqtt output is NOT enabled')

        if 'led_flow' in _sensor_config.keys():
            self.led_flow = Led(_sensor_config['led_flow'])
            self._logger.info('using GPIO pin {} as led_flow output'.format(_sensor_config['led_flow']))
        else:
            self._logger.warning('led_flow output is NOT enabled')

        pin = _sensor_config['pulse_input']
        self._logger.info('using GPIO pin {} as pulse input'.format(pin))
        self._mqtt_topic = _sensor_config['mqtt_topic']
        self._mqtt.addConnectHandler(self._on_connect)
        self._mqtt.addDisconnectHandler(self._on_disconnect)
        self.sensor = Sensor(pin=pin)
        self.sensor.when_released = self._pulse_handler
        self._mqtt.connect()
        if self.led_alive is not None:
            self.led_alive.on()

        self._logger.debug("started, mqtt publish topic: {}".format(self._mqtt_topic))

    def _on_connect(self):
        self._logger.debug('mqtt connected')
        if self.led_mqtt is not None:
            self.led_mqtt.on()

    def _on_disconnect(self, reason):
        self._logger.warning('mqtt disconnected: {}'.format(reason))
        if self.led_mqtt is not None:
            self.led_mqtt.off()

    def _pulse_handler(self, pin):
        measurement: SensorReading = SensorReading(1)
        self._logger.debug('reading: {}'.format(measurement))
        self._mqtt.publish(measurement.__dict__, self._mqtt_topic)
        if self.led_flow is not None:
            self.led_flow.flash_once(.01)

    def exit(self, reason: str):
        self._logger.warning('Exiting - {}'.format(reason))
        if self.led_alive is not None:
            self.led_alive.off()
        if self.led_mqtt is not None:
            self.led_mqtt.off()
        if self.led_flow is not None:
            self.led_flow.off()
