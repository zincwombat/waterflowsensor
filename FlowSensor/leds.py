import threading
import time
from typing import Optional
from gpiozero import LED


class OneShotTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.start()

    def start(self):
        if not self.is_running:
            self._timer = threading.Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def _run(self):
        self.is_running = False
        self.function(*self.args, **self.kwargs)

    def stop(self):
        self._timer.cancel()
        self.is_running = False


class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.next_call = time.time()
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self.next_call += self.interval
            self._timer = threading.Timer(self.next_call - time.time(), self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False


class Led:
    def __init__(self, pin: int):
        self.pin_num = pin
        self.the_led = LED(pin)
        self._event: Optional[RepeatedTimer] = None

    def on(self):
        if isinstance(self._event, RepeatedTimer):
            self._event.stop()
            self._event = None
        self.the_led.on()

    def off(self):
        if isinstance(self._event, RepeatedTimer):
            self._event.stop()
            self._event = None
        self.the_led.off()

    def toggle(self):
        self.the_led.toggle()

    def flash(self, interval):
        if isinstance(self._event, RepeatedTimer):
            self._event.stop()
        self._event = RepeatedTimer(interval, self.toggle)

    def flash_once(self, interval):
        if isinstance(self._event, RepeatedTimer):
            self._event.stop()
        if isinstance(self._event, OneShotTimer):
            self._event.stop()
        self.off()
        self.on()
        self._event = OneShotTimer(interval, self.off)
